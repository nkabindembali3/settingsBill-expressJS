const express = require('express');
const app = express();
const exphbs = require("express-handlebars");
const bodyParser = require('body-parser');
const moment = require("moment");
const SettingsBill = require("./SettingsBill");

const settingsBill = SettingsBill();

app.engine("handlebars", exphbs.engine({defaultLayout: "main"}));
app.set("view engine", "handlebars");


app.use(express.static("./public"));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get("/", function(req, res){
	res.render("index", {
		setting: settingsBill.getSettings(),
		totals: {
			smsTotal: settingsBill.totals().smsTotal.toFixed(2),
			callTotal: settingsBill.totals().callTotal.toFixed(2),
			grandTotal: settingsBill.totals().grandTotal.toFixed(2)
		},
		color: settingsBill.classTotal()
	});
});

app.post("/settings", function(req, res){
	settingsBill.setSettings(req.body);
	res.redirect("/");
});

app.post("/action", function(req, res){
	if(['sms', 'call'].includes(req.body.billItemTypeWithSettings)){
		settingsBill.recordAction(req.body.billItemTypeWithSettings);
	}
	res.redirect("/");
});


app.get("/actions", function(req, res){
	res.render("actions", {
		actions: settingsBill.actions().map(obj => {
			return {
			  type: obj.type, 
			  cost: obj.cost, 
			  timestamp: moment(new Date(obj.timestamp)).fromNow()
			}
		  })
	});
});

app.get("/actions/:type", function(req, res){
	res.render("actions", {
		actions: settingsBill.actionsFor(req.params.type).map(obj => {
			return {
			  type: obj.type, 
			  cost: obj.cost, 
			  timestamp: moment(new Date(obj.timestamp)).fromNow()
			}
		  })
	});
});

// start the app
const PORT = process.env.PORT || 3007;
app.listen(PORT, function(){
	console.log("App starting on port", PORT);
});
